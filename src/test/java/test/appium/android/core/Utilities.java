package test.appium.android.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Utilities {

	
    public static WebElement FindElement(AppiumDriver driver, String by, String element)
    {
        switch (by)
        {
            case "Id":
                return driver.findElement(By.id(element));
            case "Name":
                return driver.findElement(By.name(element));
            default:
                return null;
        }
    }
    
    public static Properties LoadConfigurationFile() throws IOException{
    	Properties config = new Properties();
		String baseDirectory=new File("").getAbsolutePath(); 
		FileInputStream file = new FileInputStream(baseDirectory+"\\src\\test\\java\\test\\appium\\android\\core\\settings\\config.properties");
		config.load(file);
		return config;
    	
    }
}
