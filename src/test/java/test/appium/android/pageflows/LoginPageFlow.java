package test.appium.android.pageflows;

import java.io.IOException;

import org.openqa.selenium.NoSuchElementException;

import io.appium.java_client.AppiumDriver;
import test.appium.android.core.BaseClass;
import test.appium.android.core.Utilities;
import test.appium.android.pageobjects.HomePage;
import test.appium.android.pageobjects.LoginPage;
import test.appium.android.testdata.data.LoginData;

public class LoginPageFlow extends BaseClass {
	
	public LoginPageFlow(AppiumDriver appiumDriver) throws IOException{
		LoginPage loginPage=new LoginPage();
		SetWebDriverInstance(appiumDriver);
	}
	
	public LoginPageFlow NavigateLoginPage() throws InterruptedException, IOException{
		
		Thread.sleep(1000);
		Utilities.FindElement(appiumDriver, "Id",LoginPage.LoginMenuProfile() ).click();
		return this;
	}
	
	
	public LoginPageFlow enterUserName(String userName) throws InterruptedException {
		Thread.sleep(1000);
		Utilities.FindElement(appiumDriver, "Id",LoginPage.UserName()).sendKeys(userName);
	    return this;
		
	}
	
	public LoginPageFlow enterPassword(String passWord) throws InterruptedException {
		Utilities.FindElement(appiumDriver, "Id",LoginPage.Password()).sendKeys(passWord);
		return this;
		
	}
	
	public LoginPageFlow clickLoginButton() throws InterruptedException{
		
		Utilities.FindElement(appiumDriver, "Id",LoginPage.LoginButton()).click();
		return this;
	}

	public LoginPageFlow loginAsUser(LoginData logindata) throws IOException, InterruptedException {
		NavigateLoginPage().enterUserName(logindata.getUserName()).enterPassword(logindata.getPassWord()).clickLoginButton();
		return this;
	}

	public boolean IsUserLoggedIn() throws IOException, InterruptedException
	{
		HomePageFlow homePageFlow= new HomePageFlow(appiumDriver);
		
		try{
			Thread.sleep(2000);
			return Utilities.FindElement(appiumDriver, "Id",HomePage.FavList()).isEnabled();
		}
		catch(NoSuchElementException e){
			return false;
		}
		
	}
}
