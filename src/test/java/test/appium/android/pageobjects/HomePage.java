package test.appium.android.pageobjects;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import test.appium.android.core.Utilities;

public class HomePage extends AbstractPage {
	
	Properties config=Utilities.LoadConfigurationFile();
	FileInputStream file = new FileInputStream(config.getProperty("ResourceFilesDirectory")+"\\HomePageResources.properties");	
	
    public HomePage() throws IOException{
    	LoadResourceFile(file);
    }
    
    public static String FavList()
    {
    	return prop.getProperty("fav_List");
    }
	

}
