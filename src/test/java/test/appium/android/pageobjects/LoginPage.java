package test.appium.android.pageobjects;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import test.appium.android.core.Utilities;



public class LoginPage extends AbstractPage {
	
	Properties config=Utilities.LoadConfigurationFile();
	FileInputStream file = new FileInputStream(config.getProperty("ResourceFilesDirectory")+"\\LoginResources.properties");		   
    public LoginPage() throws IOException {
    	LoadResourceFile(file);
    }
	
    public static String LoginMenuProfile() throws IOException
    {
      
       return prop.getProperty("loginMenuProfile");
    }
	public static String UserName()
	{
		return prop.getProperty("usernameId");
	}
	
	public static String Password()
	{
		return prop.getProperty("passwordId");
	}
	public static String LoginButton()
	{
		return prop.getProperty("loginButtonId");
	}
	
}
