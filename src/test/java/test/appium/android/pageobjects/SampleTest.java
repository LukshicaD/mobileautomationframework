package test.appium.android.pageobjects;

import io.appium.java_client.AppiumDriver;

import java.io.File;
import java.net.URL;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test
public class SampleTest {
	private WebDriver rmdriver;	

	@BeforeMethod
	public void setUp() throws Exception {
		//File classpathRoot = new File(System.getProperty("user.dir"));
		//	File appDir = new File("C://Users//lukshicad//workspace6//accountstracker//accountstracker//bin//");
		File app = new File("D:\\Research\\televisionarynew\\televisionary\\televisionay-android\\Televisionary\\bin","Televisionary.apk");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
		capabilities.setCapability(CapabilityType.VERSION, "4.4.2");
		capabilities.setCapability(CapabilityType.PLATFORM, "Windows");
		capabilities.setCapability("device", "Android Emulator");
		capabilities.setCapability("automationName", "Appium");
		capabilities.setCapability("avd", "AVD_A");
		capabilities.setCapability("app",app.getAbsolutePath());
		capabilities.setCapability("appPackage", "com.codewar.televisionary");
		//capabilities.setCapability("appWaitActivity",".MainActivity");
		capabilities.setCapability("appActivity", ".MainActivity");
		rmdriver = new AppiumDriver(new URL("http://127.0.0.1:4728/wd/hub"),capabilities);

	}
	
	@Test
	public void searchTest() throws InterruptedException {
		Thread.sleep(1000);
		JavascriptExecutor js = (JavascriptExecutor) rmdriver;
		WebElement favlist = rmdriver.findElement(By.id("com.codewar.televisionary:id/fav_list"));
		HashMap<String, Double> swipeObject = new HashMap<String, Double>();
		swipeObject.put("startX", 0.95);
	    swipeObject.put("startY", 0.5);
	    swipeObject.put("endX", 0.05);
	    swipeObject.put("endY", 0.5);
	    swipeObject.put("duration",1.8);
	    js.executeScript("mobile: swipe", swipeObject);

		
	//	Thread.sleep(3000);
	    WebElement searchmenu = rmdriver.findElement(By.id("com.codewar.televisionary:id/menu_search"));
		searchmenu.click();
		rmdriver.findElement(By.id("com.codewar.televisionary:id/et_keyword")).sendKeys("Game");
		WebElement searchbutton = rmdriver.findElement(By.id("com.codewar.televisionary:id/im_search"));
		searchbutton.click();
		WebElement title = rmdriver.findElement(By.id("com.codewar.televisionary:id/show_title_1"));
		String titleString =title.getText();
		AssertJUnit.assertTrue(titleString.contains("game"));
		
	
		
		HashMap<String, Double> tapObject = new HashMap<String, Double>();
		tapObject.put("x", (double) 426); // in pixels from left
		tapObject.put("y", (double) 286); // in pixels from top
	//	tapObject.put("element", ((RemoteWebElement) mylist).getId()); // the id of the element we want to tap
		js.executeScript("mobile: tap", tapObject);
		

		
		WebElement detailsItem= rmdriver.findElement(By.name("View Details"));
		detailsItem.click();
/*		
		WebElement favButton = rmdriver.findElement(By.id("com.codewar.televisionary:id/menu_save"));
		favButton.click();
	//	js.executeScript("mobile: tap", favButton);
		rmdriver.navigate().back();
		Thread.sleep(1000);
		rmdriver.navigate().back();
		Thread.sleep(1000);
		rmdriver.navigate().back();
		Thread.sleep(1000);

		
		 js.executeScript("mobile: swipe", swipeObject);
		 WebElement recentfeed= rmdriver.findElement(By.name("Start following game"));
		 String recentfeedStr =recentfeed.getText();
		 AssertJUnit.assertTrue(recentfeedStr.contains("Start following game"));
		 Reporter.log("Search Test Pass");
		
	//	com.codewar.televisionary:id/fav_list
	//	com.codewar.televisionary:id/show_list_view
	//	com.codewar.televisionary:id/menu_save
	*/ 
		 
	}
//	
//	@Test
//	public void searchTest2() throws InterruptedException {
//		
//		rmdriver.findElement(By.id("com.codewar.televisionary:id/menu_profile")).click();
//		Thread.sleep(500);
//		rmdriver.findElement(By.id("com.codewar.televisionary:id/et_uname")).sendKeys("lukshica_1988@yahoo.com");
//		rmdriver.findElement(By.id("com.codewar.televisionary:id/et_password")).sendKeys("aassdd");
//		rmdriver.findElement(By.id("com.codewar.televisionary:id/bt_login")).click();
//	}
//	
	@AfterMethod
	public void tearDown() throws Exception{
		rmdriver.quit();
	}
	
}
