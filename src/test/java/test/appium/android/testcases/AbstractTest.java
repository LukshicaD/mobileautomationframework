package test.appium.android.testcases;

import io.appium.java_client.AppiumDriver;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Properties;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import test.appium.android.core.Utilities;

import org.apache.commons.io.FileUtils;

public class AbstractTest {
	 protected AppiumDriver appiumDriver;
	@BeforeMethod
	public void setUp() throws Exception {
		Properties config=Utilities.LoadConfigurationFile();
		File appDir = new File(config.getProperty("appDir"));
		File app = new File(appDir, config.getProperty("appName"));
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(CapabilityType.BROWSER_NAME, config.getProperty("browserName"));
		capabilities.setCapability(CapabilityType.VERSION, config.getProperty("version"));
		capabilities.setCapability(CapabilityType.PLATFORM, config.getProperty("platform"));
		capabilities.setCapability("device", config.getProperty("device"));
		capabilities.setCapability("automationName", config.getProperty("automationName"));
		capabilities.setCapability("avd", config.getProperty("avd"));
		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("appPackage", config.getProperty("appPackage"));
		// capabilities.setCapability("appWaitActivity",".MainActivity");
		capabilities.setCapability("appActivity", config.getProperty("appActivity"));
		appiumDriver = new AppiumDriver(new URL(config.getProperty("AppiumDriverURL")), capabilities);

	}
	
	@AfterMethod
	 public void tearDown(ITestResult result) throws Exception {
	  if (!result.isSuccess()) {
	  File imageFile = ((TakesScreenshot) appiumDriver).getScreenshotAs(OutputType.FILE);
	  String FailureImageFile = result.getMethod().getMethodName() + new SimpleDateFormat("MM-dd-yyy_HH-ss").format(new GregorianCalendar().getTime())
	   + ".png";
	  File failureImageFile = new File("failureScreenshots"+"\\"+FailureImageFile);
	  FileUtils.moveFile(imageFile, failureImageFile);
	 

	  }
	  appiumDriver.quit();
	 }
}
