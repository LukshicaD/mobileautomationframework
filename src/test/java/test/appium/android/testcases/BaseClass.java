package test.appium.android.testcases;

import io.appium.java_client.AppiumDriver;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseClass {
	 protected AppiumDriver appiumDriver;
	@BeforeMethod
	public void setUp() throws Exception {
		//File classpathRoot = new File(System.getProperty("user.dir"));
		//	File appDir = new File("C://Users//lukshicad//workspace6//accountstracker//accountstracker//bin//");
		File app = new File("D:\\Research\\televisionarynew\\televisionary\\televisionay-android\\Televisionary\\bin","Televisionary.apk");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
		capabilities.setCapability(CapabilityType.VERSION, "4.4.2");
		capabilities.setCapability(CapabilityType.PLATFORM, "Windows");
		capabilities.setCapability("device", "Android Emulator");
		capabilities.setCapability("automationName", "Appium");
		capabilities.setCapability("avd", "AVD_A");
		capabilities.setCapability("app",app.getAbsolutePath());
		capabilities.setCapability("appPackage", "com.codewar.televisionary");
		//capabilities.setCapability("appWaitActivity",".MainActivity");
		capabilities.setCapability("appActivity", ".MainActivity");
		appiumDriver= new AppiumDriver(new URL("http://127.0.0.1:4728/wd/hub"),capabilities);

	}
	
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception{
		File imageFile = ((TakesScreenshot)appiumDriver).getScreenshotAs(OutputType.FILE);
		String FailureImageFile = result.getMethod().getMethodName()+ new SimpleDateFormat("MM-dd-yyy_HH-ss").format(new GregorianCalendar().getTime());
		
		appiumDriver.quit();
	}
}
