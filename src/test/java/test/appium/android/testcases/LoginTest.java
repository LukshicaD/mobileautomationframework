package test.appium.android.testcases;

import java.io.IOException;

import org.testng.annotations.Test;

import test.appium.android.pageflows.HomePageFlow;
import test.appium.android.pageflows.LoginPageFlow;
import test.appium.android.testdata.data.LoginData;

public class LoginTest extends AbstractTest{

	@Test(dataProvider="loginDataCSV" , dataProviderClass=LoginData.class)
	public void TestLogin(LoginData logindata) throws InterruptedException, IOException{
		LoginPageFlow loginPageFlow = new LoginPageFlow(appiumDriver);
		loginPageFlow.loginAsUser(logindata);
		Thread.sleep(100);
		assert loginPageFlow.IsUserLoggedIn();
	}
	
}
