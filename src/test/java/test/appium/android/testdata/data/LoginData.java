package test.appium.android.testdata.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.testng.annotations.DataProvider;

import test.appium.android.core.Utilities;
import au.com.bytecode.opencsv.CSVReader;

public class LoginData {
	
private String userName;
private String passWord;
private static CSVReader csvReader;

public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public String getPassWord() {
	return passWord;
}
public void setPassWord(String passWord) {
	this.passWord = passWord;
}


@DataProvider(name="loginDataCSV")
public static Object[][] getCSVData() throws IOException{
	
//	String csvFile = "C://Users//lukshicad//workspace6//AppiumMobileAutomation//src//main//TestData//Files//logindataCSV.csv";

    String FileName="logindataCSV.csv";
    Properties config=Utilities.LoadConfigurationFile();
	CSVReader csvReader = new CSVReader(new FileReader(config.getProperty("TestDataDirectory")+"\\"+FileName));
	java.util.List<String[]> dataList = csvReader.readAll();
	Object[][] data =  new Object[dataList.size()][1];
	ArrayList<LoginData> loginList = new ArrayList<LoginData>();
	
	for(String[] strArray:dataList){
		LoginData loginData =new LoginData();
		loginData.setUserName(strArray[0].trim());
		loginData.setPassWord(strArray[1].trim());
		loginList.add(loginData);
	}
	
	for(int i=0; i<data.length; i++){
		for(int j=0; j<data[i].length; j++ ){
			data[i][j] = loginList.get(i);
		}
	}
	
	return data;
}

}

	
	

